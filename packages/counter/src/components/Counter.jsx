import { createSignal, createEffect } from "solid-js";
import useStore from "../hooks/useStore";

import "../index.css";

const Counter = () => {
	const { store, storeSubscribe, storeIncrement } = useStore();

	const [count, setCount] = createSignal(store.count);

	createEffect(() =>
		storeSubscribe(() => {
			setCount(store.count);
		})
	);

	const isStore = store.count !== null

	return (
		<div class="counter">
			<div class="counter__text">Hello from SolidJS counter {count()}</div>
			<div class="counter__actions">
				<button class="counter__button" onClick={isStore ? storeIncrement : () => setCount(count() + 1)}>
					Click me
				</button>
			</div>
		</div>
	);
};

export default Counter;
