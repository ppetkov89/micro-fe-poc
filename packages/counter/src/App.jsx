import { render } from "solid-js/web";
import Counter from "./components/Counter";

const App = () => (
  <>
  <Counter />
  </>
);
render(App, document.getElementById("app"));
