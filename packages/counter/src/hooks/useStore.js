const useStore = () => {
	const store = window.store || { count: null };

	const { incrementCount, subscribe } = store || {};

	const storeIncrement = incrementCount ? incrementCount : () => {};
	const storeSubscribe = subscribe ? subscribe : () => {};

  return {
    store,
    storeIncrement,
    storeSubscribe
  }
}

export default useStore
