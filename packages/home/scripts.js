System.import("header").then(function (singleSpa) {
	console.log("header load");
});
System.import("single-spa").then(function (singleSpa) {
	console.log("spa load");
	singleSpa.registerApplication(
		"header",
		() => System.import("header"),
		(location) => location.pathname.startsWith("/")
	);

	singleSpa.registerApplication(
		"grid",
		() => System.import("grid"),
		(location) => location.pathname.startsWith("/")
	);
	singleSpa.start();
});

window.addEventListener("load", function () {
	console.log("load");
});
