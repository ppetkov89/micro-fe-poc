import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import Header from "./components/Header";
import Counter from "./components/Counter";
import './store.js'

const App = () => {

	return (
		<>
			<Header />
			<Counter />
		</>
	);
};

ReactDOM.render(<App />, document.getElementById("app"));
