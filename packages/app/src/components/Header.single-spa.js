import React from "react";
import ReactDOM from "react-dom";
import singleSpaReact from 'single-spa-react';
import Header from "./Header.jsx";

const reactLifecycles = singleSpaReact({
  React,
  ReactDOM,
  rootComponent:Header,
});

export const bootstrap = reactLifecycles.bootstrap;
export const mount = reactLifecycles.mount;
export const unmount = reactLifecycles.unmount;