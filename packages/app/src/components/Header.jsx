import React from "react";
import useStore from "../hooks/useStore";

import "../index.css";

const Header = () => {
	const { storeIncrement } = useStore();

	return (
		<div className="header">
			Hello from React {" "}
			<a
				onClick={() => {
					storeIncrement();
				}}
				className="button"
			>
				add to counter
			</a>
		</div>
	);
};

export default Header;
