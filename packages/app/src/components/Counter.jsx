import React, { useRef, useEffect } from "react";
import counterWrapper from "counter/counterWrapper";

const Counter = () => {
	const divRef = useRef(null);

	useEffect(() => {
		counterWrapper(divRef.current);
	}, []);

	return <div ref={divRef}></div>;
};

export default Counter;
