const glob = require("glob");
const defaultConfig = require("./webpack.config.js");
const path = require("path");

const singleSpaFiles = glob
	.sync("./src/**/*single-spa.js")
	.reduce((acc, item) => {
		const path = item.split("/");
		const name = path
			.slice(path.length - 1)[0]
			.split(".")[0]
			.toLowerCase();

		acc[name] = item;
		return acc;
	}, {});

const entry = {
	...singleSpaFiles,
};

delete defaultConfig.devServer;
delete defaultConfig.plugins;

module.exports = {
	...defaultConfig,

	output: {
		libraryTarget: "amd",
    path: path.resolve(__dirname, 'dist', 'single-spa')
	},

	entry,
};
