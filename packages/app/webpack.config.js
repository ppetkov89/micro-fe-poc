const HtmlWebPackPlugin = require("html-webpack-plugin");
const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");
const path = require("path");
const exec = require("child_process").exec;

const isProduction = process.env.NODE_ENV === 'production';
const deps = require("./package.json").dependencies;

const execBuildSpaPlugin = {
  apply: (compiler) => {
    compiler.hooks.afterEmit.tap("AfterEmitPlugin", (compilation) => {
      exec("yarn build:spa", (err, stdout, stderr) => {
        if (stdout) process.stdout.write(stdout);
        if (stderr) process.stderr.write(stderr);
      });
    });
  },
}

const plugins = [
  new ModuleFederationPlugin({
    name: "host",
    filename: "remoteEntry.js",
    remotes: {
      counter: isProduction ? "counter@https://micro-fe-counter.netlify.app/remoteEntry.js" : "counter@http://localhost:8082/remoteEntry.js",
    },
    shared: {
      ...deps,
      react: {
        singleton: true,
        requiredVersion: deps.react,
      },
      "react-dom": {
        singleton: true,
        requiredVersion: deps["react-dom"],
      },
    },
  }),
  new HtmlWebPackPlugin({
    template: "./src/index.html",
  }),
]

if (!isProduction) {
  plugins.push(execBuildSpaPlugin);
}

module.exports = {

  resolve: {
    extensions: [".tsx", ".ts", ".jsx", ".js", ".json"],
  },

  devServer: {
    port: 8080,
    historyApiFallback: true,
    static: {
			directory: path.join(__dirname, "dist"),
		},
		headers: {
			"Access-Control-Allow-Origin": "*",
		}
  },

  module: {
    rules: [
      {
        test: /\.m?js/,
        type: "javascript/auto",
        resolve: {
          fullySpecified: false,
        },
      },
      {
        test: /\.(css|s[ac]ss)$/i,
        use: ["style-loader", "css-loader", "postcss-loader"],
      },
      {
        test: /\.(ts|tsx|js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
    ],
  },

  plugins,
};
