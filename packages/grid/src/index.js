import singleSpaHtml from "single-spa-html";
import './style.css'

const template = `
<div
  class="grid"
>
  <div class="box">Hello</div>
  <div class="box">From</div>
  <div class="box">HTML</div>
</div>
`;

const jsComponent = singleSpaHtml({
	template,
});

export const bootstrap = jsComponent.bootstrap;
export const mount = jsComponent.mount;
export const unmount = jsComponent.unmount;
